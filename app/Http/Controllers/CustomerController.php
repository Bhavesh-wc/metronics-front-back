<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    public function index()
    {
        $id = auth('customer')->id();
        return view('customer.login');
    }

    public function create()
    {
        $id = auth('customer')->id();

        return view('customer.register');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|unique:customers',
            'password' => 'required|string|min:6|confirmed',
        ]);
    
        $customer = Customer::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);
    
        Auth::guard('customer')->login($customer);
    
        return redirect()->route('customer.home')->with('success','register successfully');
    }
    

    public function loginUser(Request $request)
{
    $request->validate([
        'email' => 'required|email',
        'password' => 'required|string',
    ]);

    $credentials = $request->only('email', 'password');

    if (Auth::guard('customer')->attempt($credentials)) {
        return redirect()->route('customer.home')->with('success', 'Login successfully');
    }

    $user = Customer::where('email', $request->email)->first();

    if (!$user) {
        return back()
            ->withErrors(['fail' => 'Invalid email'])
            ->withInput($request->only('email'));
    }

    if (!Hash::check($request->password, $user->password)) {
        return back()
            ->withErrors(['fail' => 'Invalid password'])
            ->withInput($request->only('email'));
    }

    return back()
        ->withErrors(['fail' => 'Both email and password are incorrect'])
        ->withInput($request->only('email'));
}


    public function home(Request $request)
    {
        return view('customer.home');
    }

    public function Logout(Request $request)
    {
        // Use the Auth facade to log out the user
       auth('customer')->logout();

        // Redirect to the login page or any other page you prefer
        return redirect('/');
    }
}
