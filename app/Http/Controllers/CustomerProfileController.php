<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Customer;
class CustomerProfileController extends Controller{
    /**
     * Display a listing of the resource.
     */
 
     public function index()
     {
         $id = auth('customer')->id();
         // Now you can use the $id variable as the authenticated customer's ID
     
         // Other logic for the index method
         return view('customer.profile');
     }
     
  

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $profile = Customer::findOrFail($id);
        
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'contactno' => 'required|digits:10',
            'name' => 'required',
            'dob' => 'required|date',
        ]);
    
        $profile->name = $request->input('name');
        $profile->contactno = $request->input('contactno');
        $profile->dob = $request->input('dob');
    
        if ($request->hasFile('image')) {
            $image_name = time() . '.' . $request->image->extension();
            $request->image->move(public_path('users'), $image_name);
            $path = 'users/' . $image_name;
            $profile->image = asset($path);
        }
    
        $profile->save();
    
        return redirect()->back()->with('success', 'Profile updated successfully');
    }
    
    public function updateEmail(Request $request, $id)
    {
        $user = Customer::find($id);

        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }
    
        $request->validate([
            'email' => ['required', 'email', 'unique:customers,email,' . $user->id ],
            'password' => 'required',
        ]);
    
        if (!Hash::check($request->input('password'), $user->password)) {
            return response()->json(['message' => 'Incorrect password'], 422);
        }
    
        $user->update(['email' => $request->input('email')]);
    
        return response()->json(['message' => 'Email updated successfully']);
    }
    
    
    public function updatePassword(Request $request , string $id)
    {
        $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        ]);
    
        $user = Customer::find($id);
    
        // Check if the current password is correct
        if (Hash::check($request->current_password, $user->password)) {
            // Update the password
            $user->password = Hash::make($request->new_password);

            $user->save();
    
            return response()->json(['message' => 'Password updated successfully']);
        } else {
            return response()->json(['message' => 'Current password is incorrect'], 422);
        }
    }
    
    
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}


