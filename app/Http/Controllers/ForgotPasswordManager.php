<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Models\Customer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str; 
class ForgotPasswordManager extends Controller
{
     function forgotPassword(){
        return view("customer.forgot-password");
     }

     function forgotPasswordPost(Request $request){
         $request->validate([
            'email'=>"required|email|exists:customers|email",
         ]);

         $token = str::random(64);
         DB::table('password_resets')->insert([
            'email'=> $request->email,
            'token'=> $token,
            'created_at'=>Carbon::now()
         ]);


         Mail::send("emails.forgot-password",['token'=>$token], function($message) use ($request){
            $message->to($request->email);
            $message->subject("Reset Password");
         });
         return redirect()->to(route("forgot.password"))->with("success","we have send an email to reset password.");
     }

   
     function resetPassword($token) {
        $email = DB::table('password_resets')
            ->where('token', $token)
            ->value('email');
    
        if ($email) {
            return view("customer.new-password", compact('token', 'email'));
        } else {
            return redirect()->route("forgot.password")->with("error", "Invalid");
        }
    }
    function resetPasswordPost(Request $request) {
        $request->validate([
            "email" => "required|email|exists:customers",
            "password" => "required|string|min:6|confirmed",
            "password_confirmation" => "required"
        ]);
    
     
        Customer::where("email", $request->email)->update([
            "password" => Hash::make($request->password)
        ]);
    
        $customer = Customer::where('email', $request->email)->first();
        auth('customer')->login($customer); 
    
     
        return redirect()->route("customer.home")->with("success", "Password reset successfully");
    }
}    
