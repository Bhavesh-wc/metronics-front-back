<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $users = User::all();  
        return view('users.index', ['user' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('users.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

            $request->validate([
                'name' => 'required|string',
                'email' => 'required|string|email|unique:users',
                'dob'=> 'required|date',
                'contactno' => 'required|digits:10',
                'password' => 'required|min:6', 
                'password_confirmation' => 'required|same:password', 
                'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            ]);
        
            $imagename = time() . '.' . $request->image->extension();
            $request->image->move(public_path('/users'), $imagename);
            $image_url = asset('users/' . $imagename);

            $user = new User;
            $user->image = $image_url;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->contactno = $request->input('contactno');
            $user->dob = $request->input('dob');
            $user->password = bcrypt($request->input('password')); 
    
            $user->save();
        
            return redirect()->route('user.index')->with('success', 'User added successfully');
        }
        
    

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user = User::find($id);
 
        return view('users.edit', compact('user'));
    }
    

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
       
        $update = User::findOrFail($id);
        
     
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'contactno' => 'required|digits:10',
            'name' => 'required',
            'dob' => 'required|date',
            'email'=>'required|email'
        ]);
    
     
        $isDataChanged = (
            $update->name !== $request->input('name') ||
            $update->contactno !== $request->input('contactno') ||
            $update->dob !== $request->input('dob') ||
            $update->email !== $request->input('email') ||
            ($request->hasFile('image') && $update->image !== $request->image)
        );
    
        if ($isDataChanged) {
            $update->name = $request->input('name');
            $update->contactno = $request->input('contactno');
            $update->dob = $request->input('dob');
            $update->email = $request->input('email');
        
            if ($request->hasFile('image')) {
                $image_name = time() . '.' . $request->image->extension();
                $request->image->move(public_path('users'), $image_name);
                $path = 'users/' . $image_name;
                $update->image = asset($path);
            }
        
            $update->save();
        
            return redirect()->route('user.index')->with('success', 'User updated successfully');
        } else {
            return redirect()->route('user.index')->with('info', 'No changes were made.');
        }
    }
    


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {

        $user = User::find($id);

        if (!$user) {
            return redirect()->route('user.index')->with('error', 'User not found');
        }
    
        $user->delete();
    
        return redirect()->route('user.index')->with('success', 'User deleted successfully');
    }
    

        }