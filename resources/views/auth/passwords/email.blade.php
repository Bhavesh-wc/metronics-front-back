@section('title', 'Reset Page')

@include('includes.head');
@section('content')

        <div class="d-flex flex-column flex-root">
            <!--begin::Authentication - Sign-in -->
            <div class="d-flex flex-column flex-lg-row flex-column-fluid">
                <!--begin::Body-->
                <div class="d-flex flex-column flex-lg-row-fluid w-lg-50 p-10 order-2 order-lg-1">
                    <!--begin::Form-->
                    <div class="d-flex flex-center flex-column flex-lg-row-fluid">
                        <!--begin::Wrapper-->
                        <div class="w-lg-500px p-10">
							@if (session('status'))
							<div class="alert alert-success custom-alert d-flex justify-content-between" role="alert">
								{{ session('status') }}
								<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

							</div>
						@endif
							<!--begin::Form-->
                            <form method="POST" action="{{ route('password.email') }}">
                                @csrf
								<div class="text-center mb-10">
									<h1 class="text-dark fw-bolder mb-3">Forgot Password ?</h1>
								
									<div class="text-gray-500 fw-semibold fs-6">Enter your email to reset your password.</div>
								</div>
							
								<div class="fv-row mb-8">
									<input type="email" placeholder="Email" name="email"
                                        value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" class="form-control bg-transparent" />
									@error('email')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror

								</div>
								<!--begin::Actions-->
								<div class="d-flex flex-wrap justify-content-center pb-lg-0">
									<button type="submit" id="kt_password_reset_submit" class="btn btn-primary me-4">
										<!--begin::Indicator label-->
										<span class="indicator-label">Submit</span>
									
										<span class="indicator-progress">Please wait...
										<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
									</button>
									<a href="{{ route('login') }}" class="btn btn-light">Cancel</a>
								</div>
							</form>
						</div>
					</div>
				</div>
		
				<div class="d-flex flex-lg-row-fluid w-lg-50 bgi-size-cover bgi-position-center order-1 order-lg-2" style="background-image: url({{ asset('admin/dist/assets/media/misc/auth-bg.png')}}">
					<!--begin::Content-->
					<div class="d-flex flex-column flex-center py-7 py-lg-15 px-5 px-md-15 w-100">
						<!--begin::Logo-->
						<a href="../../demo14/dist/index.html" class="mb-0 mb-lg-12">
							<img alt="Logo" src="{{ asset('admin/dist/assets/media/logos/custom-1.png')}}" class="h-60px h-lg-75px" />
						</a>
						<!--end::Logo-->
						<!--begin::Image-->
						<img class="d-none d-lg-block mx-auto w-275px w-md-50 w-xl-500px mb-10 mb-lg-20" src="{{ asset('admin/dist/assets/media/misc/auth-screens.png')}}" alt="" />
						<!--end::Image-->
						<!--begin::Title-->
						<h1 class="d-none d-lg-block text-white fs-2qx fw-bolder text-center mb-7">Fast, Efficient and Productive</h1>
						<!--end::Title-->
						<!--begin::Text-->
						<div class="d-none d-lg-block text-white fs-base text-center">In this kind of post,
						<a href="#" class="opacity-75-hover text-warning fw-bold me-1">the blogger</a>introduces a person they’ve interviewed
						<br />and provides some background information about
						<a href="#" class="opacity-75-hover text-warning fw-bold me-1">the interviewee</a>and their
						<br />work following this is a transcript of the interview.</div>
						<!--end::Text-->
					</div>
					<!--end::Content-->
				</div>
				<!--end::Aside-->
			</div>
			<!--end::Authentication - Password reset-->
		</div>
		<!--end::Root-->
		<!--end::Main-->
		<!--begin::Javascript-->
		<script>var hostUrl = "assets/";</script>
		<!--begin::Global Javascript Bundle(mandatory for all pages)-->
		<script src="{{ asset('admin/dist/assets/plugins/global/plugins.bundle.js')}}"></script>
		<script src="{{ asset('admin/dist/assets/js/scripts.bundle.js')}}"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Custom Javascript(used for this page only)-->
		<script src="{{ asset('admin/dist/assets/js/custom/authentication/reset-password/reset-password.js')}}"></script>
		<!--end::Custom Javascript-->
		<!--end::Javascript-->
	</body>
	<!--end::Body-->
</html>