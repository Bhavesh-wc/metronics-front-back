@extends('layouts.app')
@section('title', 'Register Page')

@section('content')


		<div class="d-flex flex-column flex-root">
			<!--begin::Authentication - Sign-up -->
			<div class="d-flex flex-column flex-lg-row flex-column-fluid">
				<!--begin::Body-->
				<div class="d-flex flex-column flex-lg-row-fluid w-lg-50 p-10 order-2 order-lg-1">
					<!--begin::Form-->
					<div class="d-flex flex-center flex-column flex-lg-row-fluid">
						<!--begin::Wrapper-->
						<div class="w-lg-500px p-10">
                            <!--begin::Form-->
                            <form method="POST" action="{{ route('register') }}"  class="form w-100" novalidate="novalidate" >
                                @if (session('fail'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{ session('fail') }}
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                                @endif
                            @csrf
                            <input type="hidden" name="roletype" value="admin">

								<!--begin::Heading-->
								<div class="text-center mb-11">
									<!--begin::Title-->
									<h1 class="text-dark fw-bolder mb-3">Sign Up</h1>
									<!--end::Title-->
									<!--begin::Subtitle-->
									<div class="text-gray-500 fw-semibold fs-6">Your Social Campaigns</div>
									<!--end::Subtitle=-->
								</div>
								<!--begin::Heading-->
								<!--begin::Login options-->
								<div class="row g-3 mb-9">
									<!--begin::Col-->
									<div class="col-md-6">
								
									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-md-6">

									</div>
									<!--end::Col-->
								</div>
								<!--end::Login options-->
								<!--begin::Input group=-->
								<div class="fv-row mb-8">
									<!--begin::name-->
									<input type="text" placeholder="Name" name="name"  value="{{ old('name') }}"class="form-control @error('name') is-invalid @enderror bg-transparent" />
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
									<!--end::name-->
								</div>
								<!--begin::Input group-->
								<!--begin::Input group=-->
                                <div class="fv-row mb-8">
									<!--begin::Email-->
									<input type="email" placeholder="Email" name="email"  value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror  bg-transparent" />

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
									<!--end::Email-->
								</div>
								<!--begin::Input group-->
								<div class="fv-row mb-8">
									<!--begin::Wrapper-->
									<div class="mb-1">
                                    <div class="fv-row mb-3">
									<!--begin::Password-->
									<input type="password" placeholder="Password" name="password"   class="form-control @error('password') is-invalid @enderror bg-transparent" />
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
									<!--end::Password-->
							
								</div>
								<!--end::Input group=-->
										
									</div>
								
								</div>
							
								<div class="fv-row mb-8">
									<!--begin::Repeat Password-->
									<input placeholder="Repeat Password" name="password_confirmation" type="password" class="form-control bg-transparent" />
                                    
									<!--end::Repeat Password-->
								</div>
								<!--end::Input group=-->
							
								<!--begin::Submit button-->
								<div class="d-grid mb-10">
									<button type="submit" class="btn btn-primary">
										<!--begin::Indicator label-->
										<span class="indicator-label">Sign up</span>
										<!--end::Indicator label-->
										<!--begin::Indicator progress-->
										<span class="indicator-progress">Please wait...
										<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
										<!--end::Indicator progress-->
									</button>
								</div>
								<!--end::Submit button-->
								<!--begin::Sign up-->
								<div class="text-gray-500 text-center fw-semibold fs-6">Already have an Account?
								<a href="{{ route('login') }}" class="link-primary fw-semibold">Sign in</a></div>
								<!--end::Sign up-->
							</form>
							<!--end::Form-->
						</div>
						<!--end::Wrapper-->
					</div>
					<!--end::Form-->
					<!--begin::Footer-->
					<div class="w-lg-500px d-flex flex-stack px-10 mx-auto">
						<!--begin::Languages-->
						<div class="me-10">
							<!--begin::Toggle-->
							
							<!--end::Menu-->
						</div>
						<!--end::Languages-->
						
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Body-->
				<!--begin::Aside-->
				<div class="d-flex flex-lg-row-fluid w-lg-50 bgi-size-cover bgi-position-center order-1 order-lg-2" style="background-image: url({{ asset('admin/dist/assets/media/misc/auth-bg.png')}})">
					<!--begin::Content-->
					<div class="d-flex flex-column flex-center py-7 py-lg-15 px-5 px-md-15 w-100">
						<!--begin::Logo-->
						<a href="../../demo14/dist/index.html" class="mb-0 mb-lg-12">
							<img alt="Logo" src="{{ asset('admin/dist/assets/media/logos/custom-1.png')}}" class="h-60px h-lg-75px" />
						</a>
						<!--end::Logo-->
						<!--begin::Image-->
						<img class="d-none d-lg-block mx-auto w-275px w-md-50 w-xl-500px mb-10 mb-lg-20" src="{{ asset('admin/dist/assets/media/misc/auth-screens.png')}}" alt="" />
						<!--end::Image-->
						<!--begin::Title-->
						<h1 class="d-none d-lg-block text-black fs-2qx fw-bolder text-center mb-7">Fast, Efficient and Productive</h1>
						<!--end::Title-->
						<!--begin::Text-->
						<div class="d-none d-lg-block text-black fs-base text-center">In this kind of post,
						<a href="#" class="opacity-75-hover text-warning fw-bold me-1">the blogger</a>introduces a person they’ve interviewed
						<br />and provides some background information about
						<a href="#" class="opacity-75-hover text-warning fw-bold me-1">the interviewee</a>and their
						<br />work following this is a transcript of the interview.</div>
						<!--end::Text-->
					</div>
					<!--end::Content-->
				</div>
				<!--end::Aside-->
			</div>
			<!--end::Authentication - Sign-up-->
		</div>
        <!--end::Root-->
		<!--end::Main-->
		<!--begin::Javascript-->
		<script>var hostUrl = "assets/";</script>
		<!--begin::Global Javascript Bundle(mandatory for all pages)-->
		<script src="{{ asset('admin/dist/assets/plugins/global/plugins.bundle.js')}}"></script>
		<script src="{{ asset('admin/dist/assets/js/scripts.bundle.js')}}"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Custom Javascript(used for this page only)-->
		<script src="{{ asset('admin/dist/assets/js/custom/authentication/sign-up/general.js')}}"></script>
		<!--end::Custom Javascript-->
		<!--end::Javascript-->
        <script src="{{ asset('admin/dist/assets/plugins/global/plugins.bundle.js')}}"></script>
        <script src="{{ asset('admin/dist/assets/js/scripts.bundle.js')}}"></script>

	</body>
	<!--end::Body-->
</html>
@endsection
