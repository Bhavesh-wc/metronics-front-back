<!DOCTYPE html>
<html>
<head>
    <style>
        .email-container {
            border: 1px solid #ccc;
            padding: 20px;
            text-align: center;
            max-width: 600px;
            margin: 0 auto;
        }
        
        .email-header {
            font-size: 24px;
            margin-bottom: 20px;
        }

        .email-content {
            font-size: 16px;
            margin-bottom: 20px;
        }

        .reset-link {
            text-decoration: none;
            background-color:#2d3748;
            padding: 10px 20px;
            border-radius: 5px;
        }
        .ii a[href] {
    color: rgb(255, 255, 255);
}
    </style>
</head>
<body>
    <div class="email-container">
        <h1 class="email-header">Reset Password</h1>
        <p class="email-content">Hello!<br>You are receiving this email because we received a password reset request for your account.</p>
        <a class="reset-link" href="{{ route('reset.password', $token) }}">Reset Password</a>
        <p class="email-content">This password reset link will expire in 60 minutes.</p>
        <p class="email-content">If you did not request a password reset, no further action is required.</p>
    </div>
</body>
</html>
