
<div class="footer py-4 d-flex flex-column flex-md-row flex-stack" id="kt_footer">
								<!--begin::Copyright-->
								<div class="text-dark order-2 order-md-1">
									
									<a class="text-gray-800 text-hover-primary">{{$config->footer ?? 'webcretacopyright©2023'}}</a>
								</div>
								<!--end::Copyright-->
								<!--begin::Menu-->
								
								<!--end::Menu-->
							</div>