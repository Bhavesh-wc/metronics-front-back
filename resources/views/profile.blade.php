@extends('layouts.default1')
@section('title', 'profile Page')
<!--begin::Head-->


@section('content')

    <head>
        <base href="" />

        <!--end::Global Stylesheets Bundle-->
        <style>
            .custom-alert {
                width: 600px;
                margin-left: 122px;
            }
            .alert {
    width: 100%;
    margin-left: 0px;
}


        </style>
    </head>

    <!--begin::Toolbar-->
    <div class="toolbar mb-5 mb-lg-7" id="kt_toolbar">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column me-3">
            <!--begin::Title-->
            <h1 class="d-flex text-dark fw-bold my-1 fs-3">Profile Settings</h1>
            <!--end::Title-->
            <!--begin::Breadcrumb-->
            <ul class="breadcrumb breadcrumb-dot fw-semibold text-gray-600 fs-7 my-1">
                <!--begin::Item-->
                <li class="breadcrumb-item text-gray-600"><a href="{{ route('admin.home') }}"
                        class="text-gray-600 text-hover-primary">Home</a> </li>
                <li class="breadcrumb-item text-gray-600">Profile</li>
                <!--end::Item-->

                <!--end::Item-->
            </ul>
            <!--end::Breadcrumb-->
        </div>
        <div class="d-flex align-items-center py-2 py-md-1">

            <a href="{{ route('admin.home') }}" class="btn btn-dark fw-bold">Back</a>
            <!--end::Button-->
        </div>
    </div>
    <!--end::Toolbar-->
    <!--begin::Post-->
    <div class="content flex-column-fluid" id="kt_content">

        <!--begin::Basic info-->
        <div class="card mb-5 mb-xl-10">
            <!--begin::Card header-->
            <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse"
                data-bs-target="#kt_account_profile_details" aria-expanded="true"
                aria-controls="kt_account_profile_details">
                <!--begin::Card title-->
                <div class="card-title m-0">
                    <h3 class="fw-bold m-0">Profile Details</h3>
                </div>
                <!--end::Card title-->
            </div>

            @if (Auth::check() && Auth::user())
            <form method="POST" action="{{ route('profile.update', Auth::user()->id) }}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                @else
                <p>User is not authenticated.</p>
            @endif

                    @if (session('success'))
                        <div class="alert alert-success  custom-alert d-flex justify-content-between " role="alert">
                            {{ session('success') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

                        </div>
                    @endif
                    @if (Session::has('fail'))
                        <div class="alert alert-success  custom-alert" id="alert"> {{ Session::get('fail') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

                        </div>
                    @endif
            
                    <!--begin::Card body-->
                    <div class="card-body border-top p-9">
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label fw-semibold fs-6">Avatar</label>
                            <div class="col-lg-8">
                                <!--begin::Image input-->
                                <div class="image-input image-input-outline" data-kt-image-input="true"
                                    style="background-image: url('{{ asset('admin/dist/assets/media/svg/avatars/blank.svg') }}">
                                    <!--begin::Preview existing avatar-->
                                    <div class="symbol symbol-125px me-5">
                                        @if (!empty(Auth::user()->image))
                                            <img src="{{ Auth::user()->image }}" alt="User Profile Image"
                                                class="image-input-wrapper w-125px h-125px">
                                        @else
                                            <div class="image-input-wrapper w-125px h-125px"
                                                style="background-image: url('{{ asset('admin/dist/assets/media/avatars/300-1.jpg') }}');">
                                            </div>
                                        @endif
                                    </div>
                                    <label
                                        class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                                        data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                        <i class="ki-duotone ki-pencil fs-7">
                                            <span class="path1"></span>
                                            <span class="path2"></span>
                                        </i>
                                        <!--begin::Inputs-->
                                        <input id="imagename" type="file" class="form-control-file" name="image">
                                    </label>

                                </div>
                            </div>
                        </div>
            
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-semibold fs-6">Full Name</label>
                          
                            <div class="col-lg-8">
                                <!--begin::Row-->
                                <div class="row">
                                    <div class="row">
                                        <!-- begin::Col -->
                                        <input type="text" name="name"
                                            class="form-control form-control-lg form-control-solid mb-3 mb-lg-0"
                                            placeholder="First name" value="{{ Auth::user()->name }}"
                                            style="width: 250px" />
                                        <!-- end::Col -->
                                        @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <!--end::Col-->
                                </div>
                                <!--end::Row-->
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-semibold fs-6">Dob</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <input type="date" name="dob" class="form-control form-control-lg form-control-solid"
                                    placeholder="Email" value="{{ Auth::user()->dob }}"
                                    style="width: 250px;margin-left:-10px" />
                                @error('dob')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--end::Col-->
                        </div>

                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-semibold fs-6">Contact no</label>
                            <div class="col-lg-8">
                                <!-- Input field -->
                                <input type="text" name="contactno"
                                    class="form-control form-control-lg form-control-solid" placeholder="Contact no"
                                    style="width: 250px; margin-left: -9px" value="{{ Auth::user()->contactno }}" />

                                <!-- Error message -->
                                @error('contactno')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!-- end::Col -->
                        </div>

                        <div class="row mb-6">

                            <div class="col-lg-8 fv-row">

                            </div>
                            <!--end::Col-->
                        </div>

                        <div class="card-footer d-flex justify-content-end py-6 px-9">

                            <button type="submit" class="btn btn-lg fw-bold btn-primary">Save Changes</button>
                        </div>
                    </div>
            
                <!--end::Actions-->
            </form>
            <!--end::Form-->
        </div>

    </div>
    <div class="card mb-5 mb-xl-10">
        <!--begin::Card header-->
        <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse"
            data-bs-target="#kt_account_signin_method">
            <div class="card-title m-0">
                <h3 class="fw-bold m-0">Sign-in Method</h3>
            </div>
        </div>
        <!--end::Card header-->
        <!--begin::Content-->
        <div id="kt_account_settings_signin_method" class="collapse show">
            <!--begin::Card body-->
            <div class="card-body border-top p-9">
                <!--begin::Email Address-->
                <!--begin::Email Address-->
                <div class="d-flex flex-wrap align-items-center">
                    <!--begin::Label-->
                    <div id="kt_signin_email">
                        <div class="fs-6 fw-bold mb-1">Email Address</div>
                        <div class="fw-semibold text-gray-600">{{ Auth::user()->email }}</div>
                    </div>
                    <!--end::Label-->
                    <!--begin::Edit-->
                    <div id="kt_signin_email_edit" class="flex-row-fluid d-none">
                        <form method="POST" action="{{ route('profile.updateEmail', Auth::user()->id) }}" id="email-form">
                            @csrf

                            <form method="POST" action="{{ route('profile.updateEmail', Auth::user()->id) }}" id="email-form">
                                @csrf
                            
                                <div class="row mb-6">
                                    <div class="col-lg-6 mb-4 mb-lg-0">
                                        <div class="fv-row mb-0">
                                            <label for="emailaddress" class="form-label fs-6 fw-bold mb-3">Enter New Email Address</label>
                                            <input type="email" name="email" class="form-control form-control-lg form-control-solid" id="emailaddress" placeholder="Email Address" value="{{ Auth::user()->email }}" />
                                            <span id="email-error" class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="fv-row mb-0">
                                            <label for="password" class="form-label fs-6 fw-bold mb-3">Confirm Password</label>
                                            <input type="password" class="form-control form-control-lg form-control-solid" name="password" id="password" />
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <button id="kt_signin_submit" type="button" class="btn btn-primary me-2 px-6">Update Email</button>
                                    <button id="kt_signin_cancel" type="button" class="btn btn-color-gray-400 btn-active-light-primary px-6">Cancel</button>
                                </div>
                            </form>
                            
                     
                        </form>
                    </div>
                    <!--end::Edit-->
                    <!--begin::Action-->
                    <div id="kt_signin_email_button" class="ms-auto">
                        <button class="btn btn-light btn-active-light-primary">Change Email</button>
                    </div>
                    <!--end::Action-->
                </div>
  
                <div class="separator separator-dashed my-6"></div>

                <div class="d-flex flex-wrap align-items-center mb-10">
                    <!--begin::Label-->
                    <div id="kt_signin_password">
                        <div class="fs-6 fw-bold mb-1">Password</div>
                    </div>
                    <!--end::Label-->
                    <!--begin::Edit-->
                    <div id="kt_signin_password_edit" class="flex-row-fluid d-none">
                        <!--begin::Form-->
                        <form method="POST" action="{{ route('profile.updatePassword', Auth::user()->id) }}"
                            id="password-form" class="form">
                            <div class="row mb-1">
                                <div class="col-lg-4">
                                    <div class="fv-row mb-0">
                                        <label for="currentpassword" class="form-label fs-6 fw-bold mb-3">Current
                                            Password</label>
                                        <input type="password" class="form-control form-control-lg form-control-solid"
                                            name="currentpassword" id="currentpassword" />
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="fv-row mb-0">
                                        <label for="newpassword" class="form-label fs-6 fw-bold mb-3">New Password</label>
                                        <input type="password" class="form-control form-control-lg form-control-solid"
                                            name="newpassword" id="newpassword"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="fv-row mb-0">
                                        <label for="confirmpassword" class="form-label fs-6 fw-bold mb-3">Confirm New
                                            Password</label>
                                        <input type="password" class="form-control form-control-lg form-control-solid"
                                            name="confirmpassword" id="confirmpassword" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-text mb-5">Password must be at least 8 character and contain symbols</div>
                            <div class="d-flex">
                                <button id="kt_password_submit" type="button" class="btn btn-primary me-2 px-6">Update
                                    Password</button>
                                <button id="kt_password_cancel" type="button"
                                    class="btn btn-color-gray-400 btn-active-light-primary px-6">Cancel</button>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>
                    <!--end::Edit-->
                    <!--begin::Action-->
                    <div id="kt_signin_password_button" class="ms-auto">
                        <button class="btn btn-light btn-active-light-primary">Reset Password</button>
                    </div>
                    <!--end::Action-->
                </div>

            </div>
            <!--end::Card body-->
        </div>
        <!--end::Content-->
    </div>

@endsection


@section('script')
    <!--end::Modal - Invite Friend-->
    <!--end::Modals-->
    <!--begin::Javascript-->
    <script>
        var hostUrl = "assets/";
    </script>
    <!--begin::Global Javascript Bundle(mandatory for all pages)-->
    <script src="{{ asset('admin/dist/assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/scripts.bundle.js') }}"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Vendors Javascript(used for this page only)-->
    <!--end::Vendors Javascript-->
    <!--begin::Custom Javascript(used for this page only)-->
    <script src="{{ asset('admin/dist/assets/js/custom/account/settings/signin-methods.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/account/settings/profile-details.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/account/settings/deactivate-account.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/pages/user-profile/general.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/widgets.bundle.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/widgets.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/apps/chat/chat.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/create-app.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/type.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/details.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/finance.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/complete.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/offer-a-deal/main.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/two-factor-authentication.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/users-search.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <!--end::Custom Javascript-->
    <!--end::Javascript-->
    <script>
    document.getElementById('kt_signin_submit').addEventListener('click', function () {
    const newEmail = document.getElementById('emailaddress').value;
    const password = document.getElementById('password').value;
    const emailError = document.getElementById('email-error');

    if (newEmail.trim() === '') {
        emailError.innerText = 'Email is required.';
        return;
    }

    // Make an AJAX request to update the email
    axios.put('{{ route('profile.updateEmail', Auth::user()->id) }}', {
        email: newEmail,
        password: password
    })
    .then(function (response) {
        Swal.fire({
            icon: 'success',
            title: 'Email Updated',
            text: response.data.message,
        }).then((result) => {
            if (result.isConfirmed) {
                location.reload();
            }
        });
        emailError.innerText = '';
    })
    .catch(function (error) {
        if (error.response && error.response.data) {
            Swal.fire({
                icon: 'error',
                title: 'Update Failed',
                text: error.response.data.message,
            });
        }
    });
});


        document.getElementById('kt_password_submit').addEventListener('click', function() {
            const currentPassword = document.getElementById('currentpassword').value;
            const newPassword = document.getElementById('newpassword').value;
            const confirmNewPassword = document.getElementById('confirmpassword').value;

            // Add your validation logic here (e.g., checking if new password matches confirm password)
            if (newPassword !== confirmNewPassword) {
                Swal.fire({
                    icon: 'error',
                    title: 'Update Failed',
                    text: 'New password and confirm password do not match',
                });
                return;
            }

            // Send an AJAX request to update the password
            axios.put('{{ route('profile.updatePassword', ['id' => Auth::user()->id]) }}', {
                    current_password: currentPassword,
                    new_password: newPassword,
                    confirm_password: confirmNewPassword,
                })
                .then(function(response) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Password Updated',
                        text: response.data.message,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            // Reload the page or redirect to a new page
                            location.reload();
                        }
                    });
                })
                .catch(function(error) {
                    if (error.response && error.response.data) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Update Failed',
                            text: error.response.data.message,
                        });
                    }
                });
        });
    </script>


@endsection
