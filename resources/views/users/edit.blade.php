@extends('layouts.default1')
@section('title', 'Create Page')
<!--begin::Head-->

@section('head')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Vendor Stylesheets(used for this page only)-->
    <link href="{{ asset('admin/dist/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet') }}"
        type="text/css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">

    <!--end::Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
    <link href="{{ asset('admin/dist/assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/dist/assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')


    <!--begin::Toolbar-->
    <div class="toolbar mb-5 mb-lg-7" id="kt_toolbar">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column me-3">
            <!--begin::Title-->
            <h1 class="d-flex text-dark fw-bold my-1 fs-3">User Form</h1>
            <!--end::Title-->
            <!--begin::Breadcrumb-->
            <ul class="breadcrumb breadcrumb-dot fw-semibold text-gray-600 fs-7 my-1">
                <!--begin::Item-->
                <li class="breadcrumb-item text-gray-600">
                    <a href="{{ route('admin.home') }}" class="text-gray-600 text-hover-primary">Home</a>
                </li>
                <!--end::Item-->
                <!--begin::Item-->
                <li class="breadcrumb-item text-gray-600">Edit User</li>

            </ul>
            <!--end::Breadcrumb-->
        </div>
        <div class="d-flex align-items-center py-2 py-md-1">

            <a href="{{ route('user.index') }}" class="btn btn-dark fw-bold">Back</a>
            <!--end::Button-->
        </div>
    </div>

    <div class="content flex-column-fluid" id="kt_content">
        <!--begin::Form-->
        <form method="post" action="{{ route('user.update', ['user' => $user->id]) }}" class="form d-flex flex-column"
            enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <!--begin::Main column-->
            <div class="d-flex flex-column flex-row-fluid gap-4 gap-lg">
                <!--begin:::Tabs-->
                <ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-semibold mb-n2">
                   
            @if (session('error'))
                <div class="alert alert-danger custom-alert d-flex justify-content-between" role="alert">
                    {{ session('error') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
       
            </ul>
            <!--begin::Input group-->

            <div class="tab-content">
                <!--begin::Tab pane-->
                <div class="tab-pane fade show active" id="kt_ecommerce_add_product_general" role="tab-panel">
                    <div class="d-flex flex-column gap-7 gap-lg-10">
                        <!--begin::General options-->
                        <div class="card card-flush py-9">
                            <div class="card-body pt-0">
                                <!--begin::Input group-->
                                <div class="mb-10 fv-row mx-10">

                                    <div class="fv-row mb-7 d-flex align-items-center">
                                        <!--begin::Label-->
                                        <label class="col-lg-4 col-form-label fw-semibold fs-6 required fs-6 mb-2" style="padding-top:12px;">Avatar</label>
                                        <div class="image-input image-input-outline" data-kt-image-input="true"
                                            style="background-image: url('{{ asset('admin/dist/assets/media/svg/avatars/blank.svg') }}">
                                            <div class="symbol symbol-125px me-5">
                                                @if (!empty($user->image))
                                                    <img src="{{ $user->image }}" alt="User Profile Image"
                                                        class="image-input-wrapper w-125px h-125px">
                                                @else
                                                    <div class="image-input-wrapper w-125px h-125px"
                                                        style="background-image: url('{{ asset('admin/dist/assets/media/avatars/300-1.jpg') }}');">
                                                    </div>
                                               
                                                @endif
                                                <label
                                                    class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                                                    data-kt-image-input-action="change" data-bs-toggle="tooltip"
                                                    title="Change avatar">
                                                    <i class="ki-duotone ki-pencil fs-7">
                                                        <span class="path1"></span>
                                                        <span class="path2"></span>
                                                    </i>
                                                    <input id="imagename" type="file" class="form-control-file"
                                                        name="image">
                                                </label>
                                            </div>
                                
                                            <div class="mt-5">
                                                @error('image')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                                </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="fv-row mb-7">
                                                <!--begin::Label-->
                                                <label class="required fw-semibold fs-6 mb-2">Full Name</label>
                                                <input type="text" name="name"
                                                    class="form-control form-control-solid mb-3" placeholder="Full name"
                                                    value="{{ old('name', $user->name) }}" />
                                                    <div class="mt-5">
                                                @error('name')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                                    </div>
                                                <!--end::Input-->
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="fv-row mb-7">
                                                <!--begin::Label-->
                                                <label class="required fw-semibold fs-6 mb-2">Email</label>
                                                <input type="email" name="email"
                                                    class="form-control form-control-solid mb-3" placeholder="Email"
                                                    value="{{ old('email', $user->email) }}" />
                                                    <div class="mt-5">
                                                        @error('email')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                        </div>
                                                <!--end::Input-->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="fv-row mb-7">
                                                <!--begin::Label-->
                                                <label class="required fw-semibold fs-6 mb-2">Contact no</label>

                                                <input type="text" name="contactno"
                                                    class="form-control form-control-solid mb-3 mb-lg-0"
                                                    placeholder="Contact no"
                                                    value="{{ old('contactno', $user->contactno) }}" />
                                                    <div class="mt-5">
                                                @error('contactno')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                                    </div>
                                                <!--end::Input-->
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="fv-row mb-7">
                                                <!--begin::Label-->
                                                <label class="required fw-semibold fs-6 mb-2">Dob</label>

                                                <input type="date" name="dob"
                                                    class="form-control form-control-solid mb-3 mb-lg-0" placeholder="Dob"
                                                    value="{{ old('dob', $user->dob) }}" />
                                                    <div class="mt-5">
                                                @error('dob')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                                    </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex mb-5"style="margin-top:48px">
                                        <a href="{{ route('user.index') }}" id="kt_ecommerce_add_product_cancel"
                                            class="btn btn-light me-5">Discard</a>
                                        <button type="submit" class="btn btn-primary">
                                            <span class="indicator-label">Update</span>
                                            <span class="indicator-progress">Please wait...
                                                <span
                                                    class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                        </button>
                                        <!--end::Button-->
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
                <!--end::Main column-->
        </form>
    </div>

    <!--end::Modal content-->


    <!--end::Post-->


@endsection
@section('script')
    <!--end::Modal - Invite Friend-->
    <!--end::Modals-->
    <!--begin::Javascript-->
    <script>
        var hostUrl = "assets/";
    </script>
    <!--begin::Global Javascript Bundle(mandatory for all pages)-->
    <script src="{{ asset('admin/dist/assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/scripts.bundle.js') }}"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Vendors Javascript(used for this page only)-->
    <script src="{{ asset('admin/dist/assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/plugins/custom/formrepeater/formrepeater.bundle.js') }}"></script>
    <!--end::Vendors Javascript-->
    <!--begin::Custom Javascript(used for this page only)-->
    <script src="{{ asset('admin/dist/assets/js/custom/apps/ecommerce/catalog/save-product.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/widgets.bundle.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/widgets.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/apps/chat/chat.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/create-app.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/users-search.js') }}"></script>
    <!--end::Custom Javascript-->
    <!--end::Javascript-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        // Use SweetAlert for confirmation when clicking the "Cancel" button
        document.querySelectorAll('#kt_ecommerce_add_product_cancel').forEach((button) => {
            button.addEventListener('click', (event) => {
                event.preventDefault();
                
                // Use the data-confirm attribute to show the confirmation message
                const confirmationMessage = button.getAttribute('data-confirm');
                
                Swal.fire({
                    title: 'Are you sure you would like to cancel?',
                    text: confirmationMessage,
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, cancel it!',
                    cancelButtonText: 'No, return'
                }).then((result) => {
                    if (result.isConfirmed) {
                        // Redirect without any further message
                        window.location = button.getAttribute('href');
                    }
                });
            });
        });
    </script>
    
@endsection
