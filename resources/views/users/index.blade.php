@extends('layouts.default1')
@section('title', 'Index Page')

@section('head')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
    <link href="{{ asset('admin/dist/assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
        type="text/css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">

    <link href="{{ asset('admin/dist/assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/dist/assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <style>
        table {
            width: 100%;
        }

        th,
        td {
            padding: 10px;
            text-align: center;
        }

        th {
            background-color: #f5f5f5;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .table:not(.table-bordered) tr:first-child,
        .table:not(.table-bordered) th:first-child,
        .table:not(.table-bordered) td:first-child {
            padding-left: 41px;
        }

        /* Override DataTables CSS to center the pagination */
        table.dataTable thead th {
            text-align: center;
            /* Center-align table header */
        }

        table.dataTable tbody td {
            text-align: center;
            /* Center-align table data */
        }

        .user-info {
            display: flex;
            align-items: center;
            text-align: center;
            margin-left: 75px;
        }

        .user-image {

            border-radius: 50%;
            overflow: hidden;
            margin-right: 10px;
        }


        .user-name {
            font-weight: bold;
        }
    </style>
@endsection

@section('content')
    <div class="toolbar mb-5 mb-lg-7">
        <!-- Toolbar content goes here -->
        <div class="page-title d-flex flex-column me-3">
            <!--begin::Title-->
            <h1 class="d-flex text-dark fw-bold my-1 fs-3">User</h1>
            <!--end::Title-->
            <!--begin::Breadcrumb-->
            <ul class="breadcrumb breadcrumb-dot fw-semibold text-gray-600 fs-7 my-1">
                <!--begin::Item-->
                <li class="breadcrumb-item text-gray-600">
                    <a href="{{ route('admin.home') }}" class="text-gray-600 text-hover-primary">Home</a>
                </li>
                                <li class="breadcrumb-item text-gray-600">Users</li>

                <li class="breadcrumb-item text-gray-600">Users</li>
            </ul>
            <!--end::Breadcrumb-->
        </div>
    </div>

    <div class="content flex-column-fluid">
        <div class="card card-flush">
            <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                <div class="card-title">
                    <div class="d-flex align-items-center position-relative my-1">
                        <input id="userSearch" type="text" class="form-control form-control-solid w-250px ps-99"
                            placeholder="Search users">

                    </div>
                </div>
                <div class="card-toolbar flex-row-fluid justify-content-end gap-5">
                    <div class="w-100 mw-150px">

                    </div>
                    <a href="{{ route('user.create') }}" class="btn btn-primary">Add Users</a>
                </div>
            </div>
            <ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-semibold mb-n2">
                <div class="card-body pt-0 px-1">
                    @if (session('error'))
                        <div class="alert alert-danger custom-alert d-flex justify-content-between"
                        style="padding-left:333px;"  role="alert">
                            {{ session('error') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif

                    @if (session('success'))
                        <div class="alert alert-success custom-alert  d-flex justify-content-between"
                            style="padding-left:333px;" role="alert">
                            {{ session('success') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif

                    @if (session()->has('fail'))
                        <div class="alert alert-danger custom-alert text-center d-flex justify-content-between"
                            style="padding-left:333px;"role="alert">
                            {{ session('fail') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif

                    @if (session('info'))
                        <div class="alert alert-info custom-alert text-center d-flex justify-content-between"
                            style="padding-left:333px;" role="alert">
                            {{ session('info') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>
            </ul>

            <table class="table table-row-dashed fs-6 gy-5" id="kt_ecommerce_products_table">
                <thead>
                    <tr class="text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                        <th>Id</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>dob</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody class="fw-semibold text-gray-600">
                    @foreach ($user as $key => $u)
                        <tr>

                            <td class="pt-14">{{ ++$key }}</td>
                            <td>
                                <div class="user-info">
                                    <div class="image-input image-input-outline" data-kt-image-input="true">
                                        <div class="symbol symbol-125px me-5">
                                            @if (!empty($u->image))
                                                <img src="{{ $u->image }}" alt="User Profile Image"
                                                    class="image-input-wrapper rounded-circle w-80px h-80px">
                                            @else
                                                <div class="image-input-wrapper rounded-circle w-80px h-80px"
                                                    style="background-image: url('{{ asset('admin/dist/assets/media/avatars/300-1.jpg') }}');">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="user-name">
                                        {{ $u->name }}
                                    </div>
                                </div>
                            </td>

                            <td>
                                <div class="user-info-details pt-9">
                                    <div class="user-email">
                                        {{ $u->email }}
                                    </div>
                            </td>
                            <td>
                                <div class="user-dob pt-9">
                                    {{ $u->dob }}
                                </div>
                            </td>
                            <td>
                                <div class="user-actions d-flex align-items-center pt-8">
                                    <a href="#"
                                        class="btn btn-light btn-active-light-primary btn-flex btn-center btn-sm"
                                        data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                                        <i class="ki-duotone ki-down fs-5 ms-1"></i>
                                    </a>
                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4"
                                        data-kt-menu="true">
                                        <div class="menu-item px-3">
                                            <a href="{{ route('user.edit', $u->id) }}" class="menu-link px-3">Edit</a>
                                        </div>

                                        <form action="{{ route('user.destroy', $u->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <div class="menu-item px-3">
                                                <button type="button"
                                                    class="menu-link px-3 btn btn-light btn-active-light-primary btn-sm delete-user"data-user-id="{{ $u->name }}">
                                                    Delete
                                                </button>
                                            </div>

                                        </form>

                                    </div>
                            </td>
        </div>

        </tr>
        @endforeach
        </tbody>

        </table>
    </div>
    </div>
    <div class="text-center">
        <div id="kt_table_1_paginate" class="dataTables_paginate paging_simple_numbers"></div>
    </div>
@endsection

@section('script')
    <script>
        var hostUrl = "assets/";
    </script>
    <script src="{{ asset('admin/dist/assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/scripts.bundle.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/widgets.bundle.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/widgets.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/apps/chat/chat.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/create-app.js') }}"></script>
    <script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/users-search.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="{{ asset('admin/dist/assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>

    <script>
        // Use SweetAlert for confirmation
        document.querySelectorAll('.delete-user').forEach((button) => {
            button.addEventListener('click', (event) => {
                event.preventDefault();
    
                const userId = event.target.getAttribute('data-user-id');
                
                Swal.fire({
                    title: 'Are you sure you want to delete ' + userId + '?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it',
                    cancelButtonText: 'No, cancel'
                }).then((result) => {
                    if (result.isConfirmed) {
                        event.target.closest('form').submit();
                    }
                });
            });
        });
    </script>
 
    
    <script>
        $(document).ready(function() {
            if (!$.fn.dataTable.isDataTable('#kt_ecommerce_products_table')) {
                $('#kt_ecommerce_products_table').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "pageLength": 4,
                    "info": false,
                    "responsive": true,
                    "ordering": false,
                });
            }

            $('#userSearch').on('keyup', function() {
                $('#kt_ecommerce_products_table').DataTable().search(this.value).draw();
            });
        });
    </script>

@endsection
