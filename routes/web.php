<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\CustomerProfileController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ForgotPasswordManager;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('admin')->group(function () {
    
    Auth::routes();
    Route::get('/home', [HomeController::class, 'index'])->name('admin.home');
    Route::post('/logout', [LoginController::class, 'adminlogout'])->name('admin.logout');
    Route::resource('profile', ProfileController::class,['only' => ['index', 'edit','update']]);
    Route::put('profile/updateEmail/{id}', [ProfileController::class, 'updateEmail'])->name('profile.updateEmail');
    Route::put('/profile/update-password/{id}',  [ProfileController::class,'updatePassword'])->name('profile.updatePassword');
Route::resource('/user',UserController::class);
});

Route::prefix('customer')->group(function () {
    Route::group(['middleware' => 'customer.guest'], function () {
      
        Route::get('/', [CustomerController::class, 'index']);
        
        Route::post('/', [CustomerController::class, 'loginUser'])->name('login-user');

        Route::get('/register', [CustomerController::class, 'create']);
        Route::post('/register', [CustomerController::class, 'store'])->name('customer.register');
        Route::get("/forgot-password", [ForgotPasswordManager::class, "forgotPassword"])->name("forgot.password");
        Route::post("/forgot-password", [ForgotPasswordManager::class, "forgotPasswordPost"])->name("forgot.password.post");
        Route::get("/reset-password/{token}", [ForgotPasswordManager::class, "resetPassword"])->name("reset.password");
        Route::post("/reset-password", [ForgotPasswordManager::class, "resetPasswordPost"])->name("reset.password.post");
    });
    Route::group(['middleware' => 'customer.auth'], function () {
        Route::get('/home', [CustomerController::class, 'home'])->name('customer.home');
        Route::get('/profile', [CustomerProfileController::class, 'index'])->name('customer.profile');
        Route::get('/profile/edit', [CustomerProfileController::class, 'edit'])->name('customer.profile.edit');
        Route::put('/profile/update/{id}', [CustomerProfileController::class, 'update'])->name('customer.profile.update');
        Route::put('/profile/updateEmail/{id}', [CustomerProfileController::class, 'updateEmail'])->name('customer.profile.updateEmail');
        Route::put('/profile/update-password/{id}', [CustomerProfileController::class, 'updatePassword'])->name('customer.profile.updatePassword');
        Route::post('/logout', [CustomerController::class, 'logout'])->name('customer.logout');
    });

});
